﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSharpDemo16
{
    public partial class frmGame : Form
    {
        private int clickCounter;
        private Random rand = new Random();
        private DateTime timeStart;

        public frmGame()
        {
            InitializeComponent();
        }

        private void btnClickMe_Click(object sender, EventArgs e)
        {
            moveButton();
            clickCounter++;
            updateText();
        }

        private void updateText()
        {
            if(1==clickCounter)
            {
                timeStart = DateTime.Now;
            }
            this.Text = "Clicked " + clickCounter + " times in " 
                + (DateTime.Now-timeStart).TotalSeconds + " seconds";
        }

        private void moveButton()
        {
            btnClickMe.Location = new Point(
                rand.Next(this.Size.Width - btnClickMe.Size.Width-30)
                , rand.Next(this.Size.Height - btnClickMe.Size.Height - 30));
        }
    }
}
